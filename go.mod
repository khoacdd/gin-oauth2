module gorm-test

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.11
)
