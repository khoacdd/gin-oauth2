package helper

import "strings"

type Response struct {
	Code    int         `json:"code"` // tag trong golang
	Message string      `json:"message"`
	Errors  interface{} `json:"errors,omitempty"`
	Data    interface{} `json:"data"`
}

type EmptyObj struct{}

func BuildResponse(code int, message string, data interface{}) Response {
	res := Response{
		Code:    code,
		Message: message,
		Data:    data,
	}
	return res
}

func BuildErrorResponse(code int, message string, err string, data interface{}) Response {
	splittedError := []string{}
	if err != "" {
		splittedError = strings.Split(err, "\n")
	}
	res := Response{
		Code:    code,
		Message: message,
		Errors:  splittedError,
		Data:    data,
	}
	return res
}
