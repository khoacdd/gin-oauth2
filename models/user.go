package models

import (
	"fmt"

	"gorm.io/gorm"
)

type User struct {
	Id       int    `json:"id"`
	FullName string `gorm:"column:fullName" json:"fullName"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
	CreateAt string `gorm:"column:createAt" json:"createAt"`
}

//create a user
func CreateUser(db *gorm.DB, User *User) (err error) {
	fmt.Println(User.Email)
	err = db.Create(User).Error
	if err != nil {
		return err
	}
	return nil
}

//get users
func GetUsers(db *gorm.DB, User *[]User) (err error) {
	err = db.Find(User).Error
	if err != nil {
		return err
	}
	return nil
}

//get user by id
func GetUser(db *gorm.DB, User *User, id string) (err error) {
	err = db.Where("id = ?", id).First(&User).Error
	if err != nil {
		return err
	}
	return nil
}

//get user bt email
func GetUserFromEmail(db *gorm.DB, User *User, email string) (err error) {
	err = db.Where("email = ?", email).First(&User).Error
	if err != nil {
		return err
	}
	return nil
}

//update user
func UpdateUser(db *gorm.DB, User *User) (err error) {
	db.Save(User)
	return nil
}

//delete user
func DeleteUser(db *gorm.DB, User *User, id string) (err error) {
	db.Where("id = ?", id).Delete(User)
	return nil
}
