package main

import (
	"gorm-test/src/modules/auth/authController"
	"gorm-test/src/modules/user/userController"

	"github.com/gin-gonic/gin"
)

func main() {
	r := setupRouter()
	_ = r.Run(":3000")
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(CORSMiddleware())

	userRepo := userController.New()
	r.POST("api/users", userRepo.CreateUser)
	r.GET("api/users/:id", userRepo.GetUser)
	r.PUT("api/users/:id", userRepo.UpdateUser)
	r.DELETE("api/users/:id", userRepo.DeleteUser)
	r.GET("api/google-login-link", authController.GetGoogleLoginLink)
	r.POST("api/google-login", authController.LoginWithGoogle)
	return r
}
