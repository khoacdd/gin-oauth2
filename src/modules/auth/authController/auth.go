package authController

import (
	"encoding/json"
	"errors"
	"fmt"
	"gorm-test/helper"
	"gorm-test/src/modules/user/userController"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"gorm.io/gorm"
)

type Credentials struct {
	ClientId     string `json:"ClientId"`
	ClientSecret string `json:"ClientSecret"`
}
type LoginLinkDTO struct {
	Link string `json:"link"`
}
type LoginDTO struct {
	Code        string `json:"code"`
	RedirectUri string `json:"redirectUri"`
}
type OAuth2UserData struct {
	Email string `json:"email"`
}

func New() *oauth2.Config {
	credentials := Credentials{
		ClientId:     "523904706657-2rjt1absqtuh8ld419k3bbdtah3o85r3.apps.googleusercontent.com",
		ClientSecret: "WZUlJInFXUCb5hHwSrmHcrcj",
	}
	var config *oauth2.Config
	config = &oauth2.Config{
		ClientID:     credentials.ClientId,
		ClientSecret: credentials.ClientSecret,
		RedirectURL:  "http://localhost:8080/google-login",
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email", // You have to select your own scope from here -> https://developers.google.com/identity/protocols/googlescopes#google_sign-in
		},
		Endpoint: google.Endpoint,
	}
	return config
}

// google login-link
func GetGoogleLoginLink(c *gin.Context) {

	config := New()

	var state string = c.Query("state")
	state = "abcabcacacac"
	var url string = config.AuthCodeURL(state, oauth2.AccessTypeOffline)
	link := LoginLinkDTO{
		Link: url,
	}
	if url == "" {
		response := helper.BuildErrorResponse(http.StatusNotFound, "Cannot get login url", "", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusNotFound, response)
		return
	}
	response := helper.BuildResponse(http.StatusOK, "success", link)
	c.JSON(http.StatusOK, response)
}

// google login-link
func LoginWithGoogle(c *gin.Context) {

	config := New()

	//get access_token from code
	loginBody := LoginDTO{
		Code:        "",
		RedirectUri: "",
	}
	c.BindJSON(&loginBody)
	accessToken, err := config.Exchange(oauth2.NoContext, loginBody.Code, oauth2.AccessTypeOffline)

	if err != nil {
		fmt.Println(err)
		response := helper.BuildErrorResponse(http.StatusBadRequest, "Cannot login, try again", "", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusOK, response)
		return
	}

	// get email from access code
	client := config.Client(oauth2.NoContext, accessToken)
	response, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		response := helper.BuildErrorResponse(http.StatusInternalServerError, "Cannot login, try again", "", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}
	defer response.Body.Close() //defer: await all function run done, excute when end of closing bracket https://gobyexample.com/defer
	data, err := ioutil.ReadAll(response.Body)
	var result OAuth2UserData
	if err := json.Unmarshal(data, &result); err != nil {
		response := helper.BuildErrorResponse(http.StatusInternalServerError, "Cannot login, try again", "", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}
	var email string = result.Email

	var userRepo = userController.New()
	user, err := userRepo.GetUserFromEmail(email)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			response := helper.BuildErrorResponse(http.StatusNotFound, "Account not exist or be deleted", "", helper.EmptyObj{})
			c.AbortWithStatusJSON(http.StatusOK, response)
			return
		}
		response := helper.BuildErrorResponse(http.StatusInternalServerError, "Cannot login, try again", "", helper.EmptyObj{})
		c.AbortWithStatusJSON(http.StatusInternalServerError, response)
		return
	}
	fmt.Println(user)
	c.JSON(http.StatusOK, user)
	//sign token va tra ve cho nguoi dung
}
